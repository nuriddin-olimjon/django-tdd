from django.http import JsonResponse


def ping(request):
    data = {"ping": "pongjon!"}
    return JsonResponse(data)
